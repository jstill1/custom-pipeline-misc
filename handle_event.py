import datetime
from datetime import timezone
import logging
import json
import os
import uuid
import sys
import re

from urllib.parse import unquote_plus

import boto3
from stratosphere_core import crypto, formatting

s3_client = boto3.client('s3')
sfn_client = boto3.client('stepfunctions')

log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s - %(process)d:%(filename)s:%(funcName)s'
logging.basicConfig(stream=sys.stdout, format=log_format)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def cleanup_lambda_handler(event, _):
    bucket = event[0]['s3_bucket']
    key = event[0]['s3_key']
    logger.info("Deleting %s/%s", bucket, key)

    # delete the original file now that we've got an encrypted version
    s3_client.delete_object(
        Bucket=bucket,
        Key=key
    )

    s3_client.delete_object(
        Bucket=bucket,
        Key=f'{key}_request'
    )

    return event[0]


def executor_lambda_handler(event, context):

    # since we'll be restarting the SFN, we'll reuse this lambda and just check the message
    base_bucket = event.get('base_bucket')
    base_key =  event.get('base_key')
    part = event.get('part')
    sfn_arn = event.get('sfn_arn')

    if not sfn_arn:
        # grab the bucket and key of the file that needs to be processed
        base_bucket = event['Records'][0]['s3']['bucket']['name']
        base_key = unquote_plus(event['Records'][0]['s3']['object']['key'])
        part = -1
        sfn_arn = os.environ['STEP_FUNCTION_ARN']

    # start the sfn
    sfn_client.start_execution(
        stateMachineArn=sfn_arn,
        # just use the requestid from context as the unique name
        name=context.aws_request_id,
        input=json.dumps({
            'base_bucket': base_bucket,
            'base_key': base_key,
            'part': part,
            'sfn_arn': sfn_arn,
            'iteration': 0  # a new exec always starts at i == 0
        })
    )


def splitter_lambda_handler(event, _):
    bucket = event['base_bucket']
    key = event['base_key']
    part = int(event['part']) + 1
    sfn_arn = event['sfn_arn']
    iteration = int(event['iteration'])
    logger.info("Splitting %s/%s part %s", bucket, key, part)

    # we'll get MEGS_PER_CHUNK MB at a time
    megs_per_chunk = int(os.environ['MEGS_PER_CHUNK'])
    range_size = megs_per_chunk * 1024 * 1024
    # start will be part * range_size - 2K
    range_start = (part * range_size) - 2048
    if range_start < 0:
        range_start = 0
    range_end = (part * range_size) + range_size

    trigger_message = encrypt_uploaded_file(bucket, key, range_start, range_end, part)

    logger.info(trigger_message)

    trigger_message.update({'sfn_arn': sfn_arn, 'iteration': iteration + 1})

    return trigger_message


def encrypt_uploaded_file(bucket, key, range_start, range_end, part):

    logger.info("Encrypting %s/%s", bucket, key)

    # read in the data
    extracted_data, has_more = read_upload_from_s3_file(bucket, key, range_start, range_end)

    # set the request_url to the s3 key
    request_url = key
    task_id = uuid.uuid5(uuid.NAMESPACE_URL, request_url).hex

    # build a request data object like analysis expects
    request_data = {
        'url': request_url,
        'headers': {'DummyHeader': 'DummyHeaderValue'},
        'timestamp': datetime.datetime.now(timezone.utc).isoformat(),
        'task_id': task_id,
        'response_status_code': '200',
    }

    # encrypt the content
    encrypted_response, encryption_key = crypto.encrypt_crawl_response(extracted_data.encode())

    # need a new key for the s3 object that goes to analysis
    # stick them in an "encrypted" prefix
    analysis_object_s3_key = str(uuid.uuid4())

    # write ecrypted response
    upload_s3_object(encrypted_response, bucket, f'encrypted/{analysis_object_s3_key}')

    # write request metadata
    upload_s3_object(json.dumps(request_data), bucket, f'encrypted/{analysis_object_s3_key}_request')

    full_page_hash = formatting.full_page_hash_from_raw_data(extracted_data)

    # delete the original file now that we've got an encrypted version
    # but only if there's not more to process
    if not has_more:
        s3_client.delete_object(
            Bucket=bucket,
            Key=key
        )

    # return the trigger message, using all the data we generated
    return {
        'task_id': task_id,
        'url': request_url,
        's3_bucket': bucket,
        's3_key': f'encrypted/{analysis_object_s3_key}',
        'encryption_key': encryption_key,
        'full_page_hash': full_page_hash,
        'page_hash': formatting.to_storable_page_hash(full_page_hash),
        'base_bucket': bucket,
        'base_key': key,
        'part': part,
        'has_more': has_more
    }


def read_upload_from_s3_file(bucket: str, object_key: str, range_start, range_end) -> dict:
    """
    Attempt to read the file at the passed key
    """
    # get the file
    s3_object = s3_client.get_object(
        Bucket=bucket,
        Key=object_key,
        Range=f'bytes={range_start}-{range_end}'
    )

    try:
        # see if we're at the end
        matches = re.match('^bytes \\d+-(?P<last_byte>\\d+)/(?P<total_bytes>\\d+)', s3_object['ContentRange']).groupdict()
        return s3_object['Body'].read().decode(), (int(matches['last_byte']) + 1 < int(matches['total_bytes']))
    except Exception:
        logger.error('Validation Error - file could not be read: %s', object_key)
        return None, False


def upload_s3_object(content: str, s3_bucket: str, s3_key: str) -> None:
    """
    Uploads a file to S3 with the passed content
    """
    # save the output file
    s3_client.put_object(
        Bucket=s3_bucket,
        Key=s3_key,
        Body=content,
        ACL='bucket-owner-full-control'
    )

    logger.debug('Stored output in %s/%s', s3_bucket, s3_key)
