import datetime
from datetime import timezone
import json
import os
import uuid

import boto3
from stratosphere_core import crypto, formatting

s3_client = boto3.client('s3')

test_storage_bucket = 'BUCKET-NAME-HERE'

def gen_data(test_file_path, dummy_url, identifier):

    base_path = os.path.dirname(test_file_path)

    # read the test file
    with open(test_file_path) as test_file:
        extracted_data = test_file.read()

    # set the request_url to the dummy we passed in
    request_url = dummy_url
    task_id = uuid.uuid5(uuid.NAMESPACE_URL, request_url).hex

    # build a request data object like analysis expects
    request_data = {
        'url': request_url,
        'headers': {'DummyHeader': 'DummyHeaderValue'},
        'timestamp': datetime.datetime.now(timezone.utc).isoformat(),
        'task_id': task_id,
        'response_status_code': '200',
    }

    # encrypt the content
    encrypted_response, encryption_key = crypto.encrypt_crawl_response(extracted_data.encode())

    # need a new key for the s3 object that goes to analysis
    analysis_object_s3_key = identifier # str(uuid.uuid4())

    # write ecrypted response
    with open(os.path.join(base_path, analysis_object_s3_key), "w") as text_file:
        text_file.write(encrypted_response)

    # write request metadata
    with open(os.path.join(base_path, f'{analysis_object_s3_key}_request'), "w") as text_file:
        text_file.write(json.dumps(request_data))

    full_page_hash = formatting.full_page_hash_from_raw_data(extracted_data)


    return json.dumps({
        'task_id': task_id,
        'url': request_url,
        's3_bucket': test_storage_bucket,
        's3_key': analysis_object_s3_key,
        'encryption_key': encryption_key,
        'full_page_hash': full_page_hash,
        'page_hash': formatting.to_storable_page_hash(full_page_hash)
    })
