#!/bin/bash
cd /app
pipenv sync
pipenv run pip install --force-reinstall git+https://oauth2:$GITLAB_TOKEN@gitlab.tb65.net/Engineering/stratosphere-core.git@v5#egg=stratosphere_core
export DEPLOYMENT_PACKAGE=/app/code.zip
pushd $(pipenv --venv)/lib/python3.7/site-packages/
zip -r9 $DEPLOYMENT_PACKAGE . -x "setuptools-*" -x "setuptools/*" -x "boto3*" -x "botocore*" -x "pip/*" -x "__pycache__/*"
popd
zip -r9 $DEPLOYMENT_PACKAGE /app/*.py